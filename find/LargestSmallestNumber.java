/*
  Find Largest and Smallest Number in an Array Example
  This Java Example shows how to find largest and smallest number in an
  array.
*/
    package find;
    public class LargestSmallestNumber {

        public static void main(String[] args) {

            //array of 10 numbers
            int numbers[] = new int[]{13,43,23,54,65,67,76,79,89,96};

            //assign first element of an array to largest and smallest
            int smallest = numbers[0];
            int largetst = numbers[0];

            for(int i=1; i< numbers.length; i++)
            {
                if(numbers[i] > largetst)
                    largetst = numbers[i];
                else if (numbers[i] < smallest)
                    smallest = numbers[i];

            }

            System.out.println("Largest Number is : " + largetst);
            System.out.println("Smallest Number is : " + smallest);
        }
    }

