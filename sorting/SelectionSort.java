package sorting;/*
Selection sort program in java
*/
import java.util.Scanner;
/*selection sort class*/
public class SelectionSort
{
    /*selection sort function*/
    public static void selectionSort(int[]array)
    {
        int i, j, index, temp;
        int N = array.length;
        for (i = 0; i < N - 1; i++)
        {
            index= i;
            for (j = i + 1; j < N; j++)
            {
                if (array[j] < array[index]) {
                    index = j;
                }
            }
        temp = array[i];
        array[i] = array[index];
        array[index] = temp;
    }
}
    /****Main method****/
    public static void main(String[] args)
    {
        int n, i;
        Scanner s = new Scanner(System.in);

        System.out.println("Enter the number of Integers to sort");
        n = s.nextInt();

        int array[] = new int[n];
        System.out.println(" Enter " + n + " Integers ");

        for(i = 0; i < n; i++)
            array[i] = s.nextInt();
        /*call method sort*/
        selectionSort(array);

        System.out.println("\nElements after sorting");
        for (i = 0; i < n; i++)
            System.out.println(" " + array[i]);
        System.out.println();
    }
}