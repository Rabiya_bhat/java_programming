package sorting;/*
*java progrsm to implemnent insertion sort
*/
import java.util.Scanner;
/*class insertion sort*/
public class InsertionSort
{
    /*Insertion Sort function*/
    public static void sort(int array[])
    {
        int N = array.length;
        int i, j, temp;
        for(i = 1; i < N; i++)
        {
            j = i;
            temp = array[i];
            while (j > 0 && temp < array[j-1])
            {
                array[j] = array[j-1];
                j = j - 1;
            }
            array[j] = temp;
        }
    }
    /*Main Method*/
    public static void main(String[] args) {
        int n, i;
        Scanner s = new Scanner(System.in);

        System.out.println("Enter the number of Integers to sort");
        n = s.nextInt();

        int array[] = new int[n];
        System.out.println(" Enter " + n + " Integers ");

        for(i = 0; i < n; i++)
            array[i] = s.nextInt();
        /*call method sort*/
        sort(array);

        System.out.println("\nElements after sorting");
        for (i = 0; i < n; i++)
            System.out.println(" " + array[i]);
        System.out.println();
    }
}
