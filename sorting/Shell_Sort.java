package sorting;

class Shell_sort {
    public static void main(String args[]) {
        int[] array = new int[] { 8,9,3,7,0,1,2,4,6,5 };

        int r, i, j, increment, temp, number_of_elements = array.length;
        /* Shell Sort Program */
        for (increment = number_of_elements / 2; increment > 0; increment /= 2)
        {
            for (i = increment; i < number_of_elements; i++)
            {
                temp = array[i];
                for (j = i; j >= increment; j -= increment)
                {
                    if (temp < array[j - increment]) {
                        array[j] = array[j - increment];
                    } else {
                        break;
                    }
                }
                array[j] = temp;
            }
        }
        System.out.println("After Sorting:");
        for (r = 0; r < 10; r++) {
            System.out.println(array[r]);
        }
    }
}